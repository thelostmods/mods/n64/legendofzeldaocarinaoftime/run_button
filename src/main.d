module main;

import crossemu.sdk.n64;
import crossemu.gdk.n64.loz_oot;

import std.stdio : writeln;

mixin(initPlugin!(
    "Run Button",
    "This mod adds a run button feature.",
    0x01_00_00_00, // 1.0.0.0,
    false
));

private // local variables
{
    Button btnRun = Button.L;
}

bool initialize()
{
    // make sure gdk loads
    if (!gdkInitialize())
        return false;
    
    // make sure we are using the expected version
    if (currentVersion != GameVersion.NTSC_1_0)
        return false;

    // everything checked out
    return true;
}

void terminate()
{
    gdkTerminate();
}

void onFirstFrame()
{
	gdkOnFirstFrame();
}

void onTick(uint frame)
{
    // process any gdk tick stuff first
    gdkOnTick(frame);

    // don't do weird stuff on title/load screens
    if (!player.exists) return;

    // escape if not holding run button
    if (!global.isButtonPressed(btnRun)) return;

    // make sure we are in a valid state to run
    if (
        player.isState(
            PlayerState.Busy, PlayerState.Climbing, PlayerState.Damaged, PlayerState.Dying,
            PlayerState.Falling, PlayerState.Shielding, PlayerState.Talking,
            PlayerState.Transforming,
            PlayerState.ChargingSword, PlayerState.ClimbingLedge, PlayerState.FirstPerson,
            PlayerState.GettingItem, PlayerState.RidingEpona, PlayerState.SceneTransition,
            PlayerState.UseItem, PlayerState.DisabledFloorCollision, PlayerState.HangingFromLedge,
            PlayerState.ClimbingOutOfWater,
            PlayerState.Crawling, PlayerState.Diving, PlayerState.Horizontal,
            PlayerState.Idle1, PlayerState.Idle2, PlayerState.Ocarina, PlayerState.Shopping,
            PlayerState.CanCrawl, PlayerState.CanRead, PlayerState.MoveSword,
            PlayerState.UnderWater, PlayerState.ConnectedToEnemy
        )
    ) return;

    // make sure we are moving
    if (!player.moved) return;

    // add half speed quarter gravity extra to normal speed
    player.posX = player.posX + player.velX / 2;
    player.posY = player.posY + player.velY / 4;
    player.posZ = player.posZ + player.velZ / 2;
}